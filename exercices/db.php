<?php

// Création de la connexion
$connexion = new PDO('mysql:host=127.0.0.1;port=3306;dbname=test', 'root', '123');

echo "erreur : " .$connexion->errorCode();

// Récupération de tous les utilisateurs
$users = $connexion->query("SELECT * FROM user")->fetchAll();
echo "<pre>";
var_dump($users);
echo "</pre>";

// Les utilisateurs
$users = [
    1 => [
        "username" => "jodeq",
        "password" => "pass1"
    ],
    2 => [
        "username" => "user2",
        "password" => "pass2"
    ]
];

/*
 * Créer une fonction qui premet de retrouver l'id d'un utilisateur
 * en passant le username et le password en paramètre
 * et faux si aucun utilisateur n'est trouvé
 */
function getUserId($username, $password) {
    global $users;

    foreach($users as $user_id => $user) {
        if ($user["username"] == $username && $user["password"] == $password)
            return $user_id;
    }

    return false;
}
