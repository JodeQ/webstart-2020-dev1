<?php
// connexion à la base de donnée
require_once('db.php');

// Récupération de la position en base
$coordinates = $db->query("SELECT latitude, longitude FROM position")->fetch();

// Envoi des coordonnées au format JSON
echo json_encode($coordinates);