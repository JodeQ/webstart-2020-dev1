# Alan's Bakery

## Installation du site

1. Configurer le fichier db.php avec les bonnes informations
2. Visitez la page http://localhost/alansbakery/setup.php (modifier l'url selon votre configuration)

Le scipt PHP de setup permet de créer la table banniere

## La bannière

il faut 2 pages :
 * la page d'accueil
 * la page d'administration

La page d'accueil (index.php) permet de visualiser la bannière :
 - Rechercher dans la base de données l'adresse de l'image
 - générer le code html avec le bon chemin pour afficher l'image (balise <img>)

la page d'administration (admin.php) contient un formulaire d'upload de la bannière :
 - une partie de traitement du formulaire qui s'active uniquement lorsue le formulaire est soumis
 - une partie d'affichage du formulaire
 - la gestion de la base de donnée pour se connecter et mettre à jour la bannière
 - une récupération de la bannière courante pour l'afficher dans ce back office
