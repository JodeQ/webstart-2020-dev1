<?php

require_once('db.php');

// Traitement de la mise à jour 
if ($_SERVER['REQUEST_METHOD'] == 'PATCH') {
    $payload = json_decode(file_get_contents('php://input'));
    if (isset($payload->id)) {
        //echo $payload->id;

        // Désactivation de toutes les bannières différentes de l'id donné
        $query = $db->prepare("UPDATE banniere SET active = 0 WHERE id != :id");
        $query->bindValue('id', $payload->id, PDO::PARAM_INT);
        $query->execute();

        // Activation de la bannière avec l'id donné
        $query = $db->prepare("UPDATE banniere SET active = 1 WHERE id = :id");
        $query->bindValue('id', $payload->id, PDO::PARAM_INT);
        $query->execute();
    }
    include_once("inc/_table_bannieres.php");
    exit();
}

// Traitement de la suppression
if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    $payload = json_decode(file_get_contents('php://input'));
    if (isset($payload->id)) {
        // Suppression de la bannière
        $query = $db->prepare("DELETE FROM banniere WHERE id = :id");
        $query->bindValue('id', $payload->id, PDO::PARAM_INT);
        $query->execute();

        // Si la bannière à supprimer est la bannière active il faut activer la bannière la plus récente
        if ($payload->isActive) {
            // Activation de la bannière la plus récente
            $query = $db->query("UPDATE banniere SET active = 1 WHERE id = (SELECT max(id) FROM banniere)");
        }
    }
    include_once("inc/_table_bannieres.php");
    exit();
}

// Traitement du formulaire d'upload
if (isset($_FILES['banniere_file'])) {
    
    // un fichier a été envoyé
    $file_data = $_FILES['banniere_file'];
    $tmp_name = $file_data['tmp_name'];
    $name = basename($file_data["name"]);

    // Déplacement du fichier
    move_uploaded_file($tmp_name, "bannieres/$name");

    // Ajout de la nouvelle bannière en base de données
    $insert = $db->prepare("INSERT INTO banniere(url) VALUES (:name)");
    $insert->bindValue(":name", $name, PDO::PARAM_STR);
    $insert->execute();

    $lastId = $db->lastInsertId();
    // Désactivation de toutes les bannières différentes de l'id donné
    $query = $db->prepare("UPDATE banniere SET active = 0 WHERE id != :id");
    $query->bindValue('id', $lastId, PDO::PARAM_INT);
    $query->execute();

    // Activation de la bannière avec l'id donné
    $query = $db->prepare("UPDATE banniere SET active = 1 WHERE id = :id");
    $query->bindValue('id', $lastId, PDO::PARAM_INT);
    $query->execute();
    

    // Redirection vers la page d'accueil
    // header('Location: index.php');
}

include_once("inc/_table_bannieres.php");

?>


<div style="margin-top: 20px;">
    <form action="" method="post" enctype="multipart/form-data">
        <div>
            <label for="banniere">Nouvelle bannière</label>
            <input id="banniere" type="file" name="banniere_file">
        </div>
        <button type="submit">Envoyer</button>
    </form>
</div>

<script>
console.log("Début du script");

function updateActive(id) {
    var xhttp = new XMLHttpRequest();
    var payload = {id: id};
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("bannieres").innerHTML = this.responseText;
        }
    }
    xhttp.open("PATCH", "admin.php", true);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.send(JSON.stringify(payload));
}

function deleteBanniere(id, active) {
    if (confirm("Supprimer cette bannière ?")) {
        var xhttp = new XMLHttpRequest();
        var payload = {id: id, isActive: active};
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("bannieres").innerHTML = this.responseText;
            }
        }
        xhttp.open("DELETE", "admin.php", true);
        xhttp.setRequestHeader('Content-Type', 'application/json');
        xhttp.send(JSON.stringify(payload));
    }
}

</script>