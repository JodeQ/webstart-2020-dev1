<?php
    require_once('db.php');

    // Récupération de toutes les bannières
    $bannieres = $db->query("SELECT * FROM banniere")->fetchAll();
?>

<table id="bannieres" style="border: solid 1px;">
    <tr>
        <th>ID</th>
        <th>url</th>
        <th>actions</th>
    </tr>
    <?php foreach($bannieres as $banniere): ?>
    <tr style="border: solid 1px;">
        <td><?php echo $banniere["id"]; ?></td>
        <td><?php echo $banniere["url"]; ?></td>
        <td>
            <input type="checkbox" name="active" <?php if ($banniere["active"] == 1) echo "checked"; ?> onchange="updateActive(<?php echo $banniere["id"]; ?>)"/>
            <span style="cursor: pointer;" onClick="deleteBanniere(<?php echo $banniere["id"]; ?>, <?php echo $banniere["active"]; ?>)">&#128465;</span>
        </td>
    </tr>
    <?php endforeach ?>
</table>