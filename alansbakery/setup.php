<?php

require_once('db.php');

if (!tableExists($db, "banniere"))
    $db->exec("CREATE TABLE banniere (id int(11) NOT NULL AUTO_INCREMENT, url varchar(255) not null, PRIMARY KEY (id))");

if (!columnExists($db, 'banniere', 'active'))
    $db->exec("ALTER TABLE banniere ADD COLUMN active TINYINT(1) DEFAULT 0");

if (!tableExists($db, "position")) {
    $db->exec("CREATE TABLE position (latitude FLOAT not null, longitude FLOAT not null)");
    $db->exec("INSERT INTO position VALUES(50.6368, 3.064)");
}

echo "<p>Setup is complete</p>";
echo '<a href="'."http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".'">Retourner à l\'accueil</a>';
