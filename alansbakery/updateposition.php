<?php

require_once('db.php');

// Récupération des coordonnées envoyées par le navigateur
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $payload = json_decode(file_get_contents('php://input'));

    if (isset($payload->latitude)) {
        $query = $db->prepare("UPDATE position SET latitude = :latitude, longitude = :longitude");
        $query->bindValue('latitude', $payload->latitude, PDO::PARAM_STR);
        $query->bindValue('longitude', $payload->longitude, PDO::PARAM_STR);
        $query->execute();
    }

    exit();
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Position</title>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>
<body>
    <div>
        <button onclick="refresh()">Rafraichir</button>
    </div>
    <script>
        var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };

        function success(pos) {
            var crd = {
                'latitude': pos.coords.latitude,
                'longitude': pos.coords.longitude,
                'accuracy': pos.coords.accuracy
            };

            console.log('Votre position actuelle est :');
            console.log(`Latitude : ${crd.latitude}`);
            console.log(`Longitude : ${crd.longitude}`);
            console.log(`La précision est de ${crd.accuracy} mètres.`);

            console.log(JSON.stringify(crd));
            // Envoyer la demande de mise à jour en ajax
            $.ajax({
                type: 'POST',
                url: 'updateposition.php',
                data: JSON.stringify(crd),
                dataType: "json",
                contentType: 'application/json'
                /* success and error handling omitted for brevity */
            });
        }

        function error(err) {
            console.warn(`ERREUR (${err.code}): ${err.message}`);
        }

        function refresh() {
            // Récupérer la position
            navigator.geolocation.getCurrentPosition(success, error, options);
        }
    </script>
</body>
</html>