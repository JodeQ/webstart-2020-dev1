<?php

define('HOST', '127.0.0.1'); // Domaine ou IP du serveur ou est située la base de données
define('PORT', 3306); // Port de connexion
define('USER', 'root'); // Nom d'utilisateur autorisé à se connecter à la base
define('PASS', '123'); // Mot de passe de connexion à la base
define('DB', 'test'); // Base de données sur laquelle on va faire les requêtes

// Tableau d'options supplémentaires pour la connexion
$db_options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8", // On force l'encodage en utf8
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, // On récupère tous les résultats en tableau associatif
    PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING // On affiche des warnings pour les erreurs, à commenter en prod (valeur par défaut PDO::ERRMODE_SILENT)
);

// On crée la connexion à la base de données
try {
    $db = new PDO('mysql:host='.HOST.';port='.PORT.';dbname='.DB, USER, PASS, $db_options);
} catch (Exception $e) {
  error_log('['.$e->getCode().'] '.$e->getMessage(), 3, 'logs/mysql-errors.log');
}

/**
 * Check if a table exists in the current database.
 *
 * @param PDO $pdo PDO instance connected to a database.
 * @param string $table Table to search for.
 * @return bool TRUE if table exists, FALSE if no table found.
 */
function tableExists(PDO $pdo, string $table) {

    // Try a select statement against the table
    // Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
    try {
        $result = $pdo->query("SELECT 1 FROM $table LIMIT 1");
    } catch (Exception $e) {
        // We got an exception == table not found
        return FALSE;
    }

    // Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
    return $result !== FALSE;
}

/**
 * Check if a column exists in a table
 *
 * @param PDO $pdo PDO instance connected to a database.
 * @param string $tableName Table to search for.
 * @param string $columnName Column to search for.
 * @return bool TRUE if column exists in the table, FALSE if no column found.
 */
function columnExists(PDO $db, string $tableName, string $columnName) : bool {
    // Test de l'existence de la colonne dans la table
    $column = $db->query("
    SELECT column_name 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_schema='".DB."' 
    AND table_name='$tableName' 
    AND column_name='$columnName'
    ")->fetch();

    return (!$column) ? false : true;
}