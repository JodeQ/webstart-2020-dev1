<?php
// Connexion à la base de données
require_once('db.php');

// ici à faire récupération du chemin de la bannière
$banniere_url = $db->query("select url from banniere where active = 1 order by id desc limit 1")->fetch()["url"];

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alan's Bakery</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>
<body>
    <!-- Ici il faut modifier le chemin dans src avec le bon nom d'image -->
    <img src="bannieres/<?php echo $banniere_url; ?>" style="width: 80%; max-height: 300px;">

    <div id="mapid" style="height: 250px; width: 500px;">

    </div>

    <script>

        var coordinates;
        var mymap;
        var marker;

        function initMap(coordinates) {
            mymap = L.map('mapid').setView([coordinates.latitude, coordinates.longitude], 13);
            L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
                // Il est toujours bien de laisser le lien vers la source des données
                attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
                minZoom: 1,
                maxZoom: 20
            }).addTo(mymap);

            // Ajouter un marqueur pour l'école webstart
            var webstart = L.marker([50.633690098294146, 3.064603556033135]).addTo(mymap);
            webstart.bindPopup("<b>Hello world!</b><br>I am a popup.");

            marker = L.marker([coordinates.latitude, coordinates.longitude]).addTo(mymap);
            marker.bindPopup("<b>Nous sommes ici</b>").openPopup();
        }

        // Récupérer les coordonnées de départ de la carte
        $.ajax("position.php")
        .done(function(data) {
            if (console && console.log ) {
                coordinates = JSON.parse(data);
		        initMap(coordinates);
            }
        });

    </script>
</body>
</html>